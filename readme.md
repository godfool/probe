# Probe

Moved to https://github.com/ckoehler/probe

[![pipeline status](https://gitlab.com/godfool/probe/badges/master/pipeline.svg)](https://gitlab.com/godfool/probe/-/commits/master)

Probe is a ZMQ PUB/SUB monitor built with TUI.


![screenshot](assets/screen1.png)

# Keyboard Shortcuts

| Key       | Action                          |
| ----      | -----                           |
| q         | Quit                            |
| h, Left   | Previous Tab                    |
| l, Right  | Next Tab                        |
| j         | Next Probe                      |
| k         | Previous Probe                  |
| \<Enter\> | Show Details for selected probe |
