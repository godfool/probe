# Changelog

All notable changes to this project are documented in this file.

The format is based on [Keep a Changelog], and this project adheres to
[Semantic Versioning]. The file is auto-generated using [Conventional Commits].

[keep a changelog]: https://keepachangelog.com/en/1.0.0/
[semantic versioning]: https://semver.org/spec/v2.0.0.html
[conventional commits]: https://www.conventionalcommits.org/en/v1.0.0/

## Overview

- [unreleased](#unreleased)
- [`0.1.0`](#010) – _2021.03.24_

## _[Unreleased]_

- feat: first step to run as Docker container ([`97ee399`])
- refactor: simplify code ([`e8a6ebb`])

## [0.1.0] – _0.1.0_

_2021.03.24_

0.1.0


### Changes

<!-- [releases] -->

[unreleased]: #/compare/v0.1.0...HEAD
[0.1.0]: #/releases/tag/v0.1.0

<!-- [commits] -->

[`97ee399`]: #/commit/97ee39905eed5b200e558b5d03116fab62fd3199
[`e8a6ebb`]: #/commit/e8a6ebbc6571a69c0c0ec006c9b8bb0f798534b6
